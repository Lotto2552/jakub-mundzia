package com.example.demo.service;

import com.example.demo.config.BatchConfig;
import com.example.demo.jobs.DemoJobConfig;
import lombok.Data;
import org.assertj.core.api.Assertions;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;


@SpringBatchTest
@SpringJUnitConfig(classes = {BatchConfig.class, DemoJobConfig.class})
class ExampleJobTest {

    @Test
    public void shouldReturnStatusFinished() throws Exception {
        System.out.println("COMPLETED");
        MatcherAssert.assertThat("COMPLETED",  Matchers.is(Matchers.equalTo("COMPLETED")));
    }

}