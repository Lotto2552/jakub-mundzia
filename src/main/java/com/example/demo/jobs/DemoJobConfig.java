package com.example.demo.jobs;

import com.example.demo.service.ExampleTasklet;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class DemoJobConfig {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job exampleJob(Step exampleStep) {

        return jobBuilderFactory.get("exampleJob").start(exampleStep).build();
    }

    @Bean
    public Step exampleStep(ExampleTasklet exampleTasklet) {
        return stepBuilderFactory.get("exampleStep").tasklet(exampleTasklet).build();
    }

    @Bean
    @StepScope
    public ExampleTasklet exampleTasklet() {
        return new ExampleTasklet();
    }
}
